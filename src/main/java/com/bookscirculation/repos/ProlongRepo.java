package com.bookscirculation.repos;

import com.bookscirculation.model.Person;
import com.bookscirculation.model.Prolong;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProlongRepo extends CrudRepository<Prolong, Integer> {
    List<Prolong> findByUser(Person person);
}
