package com.bookscirculation.repos;

import com.bookscirculation.model.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface BookRepo extends CrudRepository<Book, Integer> {
    Optional<Book> findById(Integer id);

    List<Book> findByUserId(Integer userId);

    List<Book> findAll();


    List<Book> findAllByReturnDateAndAvailable(String date, boolean available);

    @Query("select c from Book c where c.user.id <> :id ORDER BY c.id DESC")
    List<Book> findByOrderByIdDescWhereUserIsNotCurrent(@Param("id") Integer id);

    @Query("select c from Book c where c.available = :available and c.user.id <> :id ORDER BY c.id DESC")
    List<Book> findByAvailableOrderByIdDescWhereUserIsNotCurrent(@Param("available") boolean available, @Param("id") Integer id);

    @Query("select c from Book c where c.user.id <> :id and lower(c.name) like lower(concat('%', :name,'%')) ORDER BY c.id DESC")
    List<Book> findByNameOrderByIdDescWhereUserIsNotCurrent(@Param("name") String name, @Param("id") Integer id);

    @Query("select c from Book c where c.available = :available and c.user.id <> :id and lower(c.name) like lower(concat('%', :name,'%')) ORDER BY c.id DESC")
    List<Book> findByNameAndAvailableOrderByIdDescWhereUserIsNotCurrent(@Param("name") String name, @Param("available") boolean available, @Param("id") Integer id);
}
