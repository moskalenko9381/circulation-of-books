package com.bookscirculation.repos;

import com.bookscirculation.model.ChatMessage;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChatMessageRepo extends CrudRepository<ChatMessage, Long> {
    List<ChatMessage> findAll();
    List<ChatMessage> findAllByDealId(Integer dealId);
}