package com.bookscirculation.repos;

import com.bookscirculation.model.Person;
import org.springframework.data.repository.CrudRepository;


public interface PersonRepo extends CrudRepository<Person, Integer> {
    Person findByEmail(String email);

    Person findByActivationCode(String code);
}
