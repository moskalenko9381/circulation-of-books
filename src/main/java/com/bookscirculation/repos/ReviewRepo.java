package com.bookscirculation.repos;

import com.bookscirculation.model.Person;
import com.bookscirculation.model.Review;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReviewRepo extends CrudRepository<Review, Integer> {
    List<Review> findByRecipient(Person person);

    List<Review> findByWriter(Person person);
}
