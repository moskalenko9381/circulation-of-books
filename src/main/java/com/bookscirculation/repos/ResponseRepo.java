package com.bookscirculation.repos;

import com.bookscirculation.model.BookRequest;
import com.bookscirculation.model.ResponseToRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ResponseRepo extends CrudRepository<ResponseToRequest, Integer> {
    ResponseToRequest findByBookRequest(BookRequest request);

    @Query("select c from ResponseToRequest c where c.initiatorsBook.id = :id")
    List<ResponseToRequest> findByInitiatorsBookId(@Param("id") Integer id);

}
