package com.bookscirculation.repos;

import com.bookscirculation.model.BookRequest;
import com.bookscirculation.model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRequestRepo extends CrudRepository<BookRequest, Integer> {

    List<BookRequest> findByPersonInitiator(Person personInitiator);

    @Query("select c from BookRequest c where c.respondentsBook.user.id = :id")
    List<BookRequest> findByIdOwner(@Param("id") Integer id);

    @Query("select c from BookRequest c where c.respondentsBook.id = :id")
    List<BookRequest> findByRespondentsBookId(@Param("id") Integer id);

    @Query("select c from BookRequest c where c.respondentsBook.user.id = :id and c.status = :status")
    List<BookRequest> findFirstRequestByIdOwnerAndStatus(@Param("id") Integer id, @Param("status") String status);

}
