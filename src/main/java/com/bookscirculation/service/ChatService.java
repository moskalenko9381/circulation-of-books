package com.bookscirculation.service;

import com.bookscirculation.model.Deal;
import com.bookscirculation.model.Person;
import com.bookscirculation.repos.ChatMessageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

@Service
public class ChatService {

    @Autowired
    MailSender mailSender;

    @Autowired
    ChatMessageRepo chatMessageRepo;

    public void sendMailChat(Person person, Deal deal, String content) {
        if (!StringUtils.isEmpty(person.getEmail()) && person.isNotice()) {
            String message =
                    "Hello, " + person.getFirstname() + "\nYou have a new message in chat of deal " + deal.getRespondentsBook().getName()
                            + " on " + deal.getInitiatorsBook().getName() + "\n'" + content + "'";
            mailSender.send(person.getEmail(), "New message in chat", message);
        }
    }
}
