package com.bookscirculation.service;

import com.bookscirculation.model.Person;
import com.bookscirculation.repos.BookRequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;


@Service
public class BookRequestService {

    @Autowired
    BookRequestRepo bookRequestRepo;

    @Autowired
    MailSender mailSender;

    @Value("${url}")
    private String url;

    public void sendMailRequest(Person person) {
        if (!StringUtils.isEmpty(person.getEmail()) && person.isNotice()) {
            String message = "Hello," + person.getFirstname()+
                    "\nYou have a new exchange request. Look at the requests to me on the next link: " + url + "requests_to_me";
            mailSender.send(person.getEmail(), "New request", message);

        }
    }


}
