package com.bookscirculation.service;


import com.bookscirculation.model.Book;
import com.bookscirculation.model.BookRequest;
import com.bookscirculation.model.Person;
import com.bookscirculation.model.ResponseToRequest;
import com.bookscirculation.repos.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;


@Service
@Slf4j
public class ResponseService {

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private DealService dealService;

    @Autowired
    private DealRepo dealRepo;

    @Autowired
    private PersonService personService;

    @Autowired
    private ResponseRepo responseRepo;

    @Autowired
    private BookRequestRepo bookRequestRepo;

    @Value("${url}")
    private String url;


    public void createResponseWithReject(Integer id) {
        int userId = personService.getCurrentId();
        Person user = personRepo.findById(userId).get();
        BookRequest request = bookRequestRepo.findById(id).get();
        ResponseToRequest response = new ResponseToRequest();
        response.setStatus(false);
        response.setBookRequest(request);
        response.setPersonRespondent(user);
        responseRepo.save(response);
        log.debug(response.toString());
        request.setStatus("rejected");
        bookRequestRepo.save(request);
        log.debug(request.toString());
    }


    public void createAgreeResponse(Integer reqId, Integer bookId, ResponseToRequest response) {
        BookRequest req = bookRequestRepo.findById(reqId).get();
        Book book = bookRepo.findById(bookId).get();
        Person user = personRepo.findById(personService.getCurrentId()).get();

        response.setStatus(true);
        response.setBookRequest(req);
        response.setPersonRespondent(user);
        response.setInitiatorsBook(book);

        responseRepo.save(response);
        log.debug(response.toString());
        req.setStatus("agree");
        bookRequestRepo.save(req);
        log.debug(req.toString());
        dealService.createDeal(book, req, response);
    }

    @Autowired
    MailSender mailSender;

    public void sendMailResponse(Person person) {
        if (!StringUtils.isEmpty(person.getEmail()) && person.isNotice()) {
            String message =
                    "Hello, " + person.getFirstname() +
                            ", you have a new response. Look at the responses" +
                            " to me on the next link: " + url + "responses_to_me";

            mailSender.send(person.getEmail(), "New response", message);

        }
    }
}
