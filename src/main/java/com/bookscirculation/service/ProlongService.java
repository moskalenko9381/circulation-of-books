package com.bookscirculation.service;

import com.bookscirculation.model.Deal;
import com.bookscirculation.model.Person;
import com.bookscirculation.model.Prolong;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.repos.ProlongRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Slf4j
public class ProlongService {

    @Autowired
    ProlongRepo prolongRepo;

    @Autowired
    DealRepo dealRepo;

    @Autowired
    PersonService personService;

    @Autowired
    PersonRepo personRepo;

    @Autowired
    private MailSender mailSender;

    public void addProlong(Integer deal, String date) {
        Prolong prolong = new Prolong();
        prolong.setNewReturnDate(LocalDate.parse(date));
        Deal curDeal = dealRepo.findById(deal).get();
        prolong.setDeal(curDeal);
        Person curUser = personRepo.findByEmail(personService.currentEmail());
        if (curDeal.getInitiatorsBook().getUser() == curUser)
            prolong.setUser(curDeal.getRespondentsBook().getUser());
        else prolong.setUser(curDeal.getInitiatorsBook().getUser());
        prolongRepo.save(prolong);
    }

    public void acceptProlong(Integer id) {
        Prolong curProlong = prolongRepo.findById(id).get();
        Deal deal = curProlong.getDeal();
        if (deal.getReturnDate().isBefore(curProlong.getNewReturnDate())) {
            deal.setReturnDate(curProlong.getNewReturnDate());
            log.info("New return day of deal: " + curProlong.getNewReturnDate());
            String message = "New deadline of exchange (" + deal.getInitiatorsBook().getName()
                    + " on " + deal.getRespondentsBook().getName() + "):" + deal.getReturnDate();
            if (deal.getInitiatorsBook().getUser().isNotice()) {
                mailSender.send(deal.getInitiatorsBook().getUser().getEmail(), "New deadline of exchange", message);
            }
            if (deal.getRespondentsBook().getUser().isNotice()) {
                mailSender.send(deal.getRespondentsBook().getUser().getEmail(), "New deadline of exchange", message);
            }
        }
        prolongRepo.delete(curProlong);
    }
}
