package com.bookscirculation.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class Deal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "initiators_book_id", referencedColumnName = "id")
    private Book initiatorsBook;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "respondent_book_id", referencedColumnName = "id")
    private Book respondentsBook;

    @Basic
    private java.time.LocalDate ReturnDate;

    @Column
    private boolean delivery;

    private String status;

    @Column(name = "close_deal_init")
    private boolean closeDealByInitiator = false;

    @Column(name = "close_deal_resp")
    private boolean closeDealByRespondent = false;

    public boolean isWait() {
        return status.equals("wait");
    }
}
