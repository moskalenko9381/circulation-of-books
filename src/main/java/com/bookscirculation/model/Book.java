package com.bookscirculation.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "book")
@Entity
@Builder
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Person user;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "author")
    private String author;

    @Min(1)
    @Max(10)
    private Short condition;

    @Column(name = "description", length = 1000, nullable = false)
    private String description;

    @DateTimeFormat
    @Column(name = "added_date", nullable = false)
    private Date addedDate = new java.sql.Date(new java.util.Date().getTime());

    @DateTimeFormat
    @Column(name = "return_date")
    private String returnDate;

    @Column
    private boolean available;

    @Column(length = 64)
    private String photo;

    @Transient
    public String getPhotosImagePath() {
        if (photo == null || id == null || photo.isEmpty()) return null;

        return "/user-photos/" + id + "/" + photo;
    }
}
