package com.bookscirculation.model;

public enum Provider {
    LOCAL, GOOGLE
}
