package com.bookscirculation.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseToRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "request_id", referencedColumnName = "id")
    private BookRequest bookRequest;

    @Column(nullable = false)
    private boolean status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book1_id", referencedColumnName = "id")
    private Book initiatorsBook;

    @Column
    private boolean bookDelivery;

    @Column
    private String note;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "respondent_user_id", referencedColumnName = "id")
    private Person personRespondent;

}
