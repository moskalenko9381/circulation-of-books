package com.bookscirculation.controller;

import com.bookscirculation.model.ChatMessage;
import com.bookscirculation.model.Deal;
import com.bookscirculation.repos.ChatMessageRepo;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@Controller
@Slf4j
public class ChatController {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private ChatMessageRepo chatMessageRepo;

    @Autowired
    private DealRepo dealRepo;

    private String curEmail;

    private Deal curDeal;


    @GetMapping("/chat/{dealId}")
    public String getChat(Model model, @PathVariable Integer dealId) {

        if (!dealRepo.findById(dealId).isPresent())
            return "redirect:/deals";

        curDeal = dealRepo.findById(dealId).get();

        if (!(curDeal.getInitiatorsBook().getUser().getEmail().equals(personService.currentEmail()) ||
                curDeal.getRespondentsBook().getUser().getEmail().equals(personService.currentEmail())))
            return "redirect:/deals";
        curEmail = personService.currentEmail();

        List<ChatMessage> messages = chatMessageRepo.findAllByDealId(curDeal.getId());
        model.addAttribute("messages", messages);
        model.addAttribute("deal", curDeal);
        model.addAttribute("name", personRepo.findByEmail(personService.currentEmail()).getFullName());
        model.addAttribute("userEmail", personService.currentEmail());

        String chat = curDeal.getInitiatorsBook().getName() + " to " + curDeal.getRespondentsBook().getName();
        model.addAttribute("chatId", chat);
        return "chat";

    }


    // all messages from client starting from /app

    // /app/chat/{roomId}/sendMessage
    @MessageMapping("/chat/{roomId}/sendMessage")
    public void sendMessage(@DestinationVariable String roomId, @Payload ChatMessage chatMessage) {
        log.debug("New message");
        log.debug(chatMessage.toString());
        chatMessage.setSenderEmail(curEmail);
        chatMessage.setDealId(curDeal.getId());
        chatMessageRepo.save(chatMessage);

        messagingTemplate.convertAndSend("/channel/" + roomId, chatMessage);
    }


    @MessageMapping("/chat/{roomId}/addUser")
    public void addUser(@DestinationVariable String roomId, @Payload ChatMessage chatMessage,
                        SimpMessageHeaderAccessor headerAccessor) {
        String currentRoomId = (String) headerAccessor.getSessionAttributes().put("room_id", roomId);

        if (currentRoomId != null) {
            ChatMessage leaveMessage = new ChatMessage();
            leaveMessage.setSenderEmail(chatMessage.getSenderEmail());
            messagingTemplate.convertAndSend("/channel/" + currentRoomId, leaveMessage);
        }

        headerAccessor.getSessionAttributes().put("username", chatMessage.getSenderName());

        if (chatMessage.getContent() != null)
            messagingTemplate.convertAndSend("/channel/" + roomId, chatMessage);
    }
}