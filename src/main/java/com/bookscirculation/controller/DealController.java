package com.bookscirculation.controller;


import com.bookscirculation.model.Deal;
import com.bookscirculation.model.Review;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.repos.ReviewRepo;
import com.bookscirculation.service.DealService;
import com.bookscirculation.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
public class DealController {

    @Autowired
    DealRepo dealRepo;

    @Autowired
    DealService dealService;

    @Autowired
    PersonService personService;

    @Autowired
    PersonRepo personRepo;

    @Autowired
    ReviewRepo reviewRepo;

    @GetMapping("/responses_to_me")
    public String getPossibleDeals(Model model) {
        Integer id = personService.getCurrentId();
        List<Deal> possibleDeals = dealRepo.findByInitiatorAndStatus(id, "wait");
        model.addAttribute("deals", possibleDeals);
        int userId = personService.getCurrentId();
        model.addAttribute("userId", userId);
        return "responses_to_my_request";
    }

    @GetMapping("/start_deal/{dealId}/")
    public String startDeal(@PathVariable Integer dealId) {
        Deal deal = dealRepo.findById(dealId).get();
        deal.setStatus("active");
        deal.getInitiatorsBook().setAvailable(false);
        deal.getRespondentsBook().setAvailable(false);
        dealRepo.save(deal);
        return "redirect:/responses_to_me";
    }

    @GetMapping("/reject_deal/{dealId}")
    public String rejectDeal(@PathVariable Integer dealId) {
        Deal deal = dealRepo.findById(dealId).get();
        deal.setStatus("rejected");
        dealRepo.save(deal);
        return "redirect:/responses_to_me";
    }

    @GetMapping("/deals")
    public String deals(Model model) {
        int id = personService.getCurrentId();
        List<Deal> deals = dealRepo.findByInitiatorOrRespondentAndStatus(id, "active");
        int userId = personService.getCurrentId();
        model.addAttribute("userId", userId);
        model.addAttribute("deals", deals);
        return "deals";
    }

    @GetMapping("/completed_deals")
    public String completedDeals(Model model) {
        int id = personService.getCurrentId();
        List<Deal> deals = dealRepo.findByInitiatorOrRespondentAndStatus(id, "completed");
        model.addAttribute("deals", deals);
        return "completed_deals";
    }

    @GetMapping("/end_deal/{id}")
    public String endDeal(@PathVariable Integer id) {
        Deal deal = dealRepo.findById(id).get();
        Integer id1 = deal.getRespondentsBook().getUser().getId();
        Integer id2 = deal.getInitiatorsBook().getUser().getId();
        dealService.closeDeal(id);
        return ("redirect:/review/" + id1.toString() + "/" + id2.toString());
    }

    @GetMapping("/review/{id1}/{id2}")
    public String addReview(@PathVariable Integer id1, @PathVariable Integer id2, Model model) {
        model.addAttribute("review", new Review());
        model.addAttribute("idf", id1);
        model.addAttribute("ids", id2);
        return "leave_review";
    }

    @PostMapping("/review/{id1}/{id2}")
    public String sendReview(@PathVariable Integer id1, @PathVariable Integer id2, @Valid Review review, Model model) {
        personService.sendReview(id1, id2, review);
        reviewRepo.save(review);
        return "redirect:/deals";
    }

}
