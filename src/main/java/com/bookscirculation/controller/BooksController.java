package com.bookscirculation.controller;

import com.bookscirculation.model.Book;
import com.bookscirculation.model.Deal;
import com.bookscirculation.model.Person;
import com.bookscirculation.model.Review;
import com.bookscirculation.repos.BookRepo;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.repos.ReviewRepo;
import com.bookscirculation.service.BookService;
import com.bookscirculation.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
@CrossOrigin(origins = "*")
public class BooksController {

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private BookService bookService;

    @Autowired
    private PersonService personService;

    @Autowired
    DealRepo dealRepo;

    @Autowired
    ReviewRepo reviewRepo;

    @GetMapping("/books")
    public String booksPage(Model model, @RequestParam(required = false) String name,
                            @RequestParam(defaultValue = "false") boolean checkbox,
                            @RequestParam(defaultValue = "false") boolean delivery,
                            @RequestParam(required = false) String city) {
        if (city == null || city.equals("")) city = "";
        List<Book> allBooks = bookService.getBooksForMain(checkbox, name, delivery, city);
        model.addAttribute("selectedCity", city);
        model.addAttribute("showFlag", checkbox);
        model.addAttribute("showFlagDelivery", delivery);
        model.addAttribute("person", personRepo.findByEmail(personService.currentEmail()));
        model.addAttribute("books", allBooks);
        return "books";
    }


    @GetMapping("/add_book")
    public String signUp(Model model) {
        model.addAttribute("newBook", new Book());
        return "add_book";
    }

    @PostMapping("/add_book")
    public String addBook(@Valid Book newBook,
                          @RequestParam("image") MultipartFile multipartFile,
                          BindingResult bindingResult, Model model)
            throws IOException {

        if (bindingResult.hasErrors()) {
            log.info(newBook.toString());
            model.addAttribute("newBook", newBook);
            return "add_book";
        }

        int userId = personService.getCurrentId();
        log.debug("Current user id for add book " + userId);
        Person person = personRepo.findById(userId).get();
        newBook.setUser(person);
        newBook.setAvailable(true);

        bookService.addPhotoBook(multipartFile, newBook);
        return "redirect:/books";
    }

    @GetMapping("/books/delete/{id}")
    public String delete(@PathVariable Integer id) {
        List<Deal> dealsWithBook = dealRepo.findByBookIdAndStatus(id, "active");
        if (dealsWithBook.isEmpty()) {
            int deleteId = personService.getCurrentId();
            Optional<Book> val = bookRepo.findById(id);
            if (val.isPresent() && val.get().getUser().getId() == deleteId) {
                bookService.deleteBook(id);
            }
        }
        return "redirect:/mybooks";
    }


    @GetMapping("/books/edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
        Book book = bookRepo.findById(id).get();
        model.addAttribute("book", book);
        return "edit_book";
    }

    @PostMapping("/books/edit/{id}")
    public String addBook(@PathVariable Integer id,
                          @Valid Book book, @RequestParam("image") MultipartFile multipartFile,
                          BindingResult bindingResult, Model model) throws IOException {

        if (bindingResult.hasErrors()) {
            log.info(book.toString());
            model.addAttribute("book", book);
            return "edit_book";
        }
        int userId = personService.getCurrentId();
        Person person = personRepo.findById(userId).get();

        bookService.editPhotoBook(multipartFile, book);

        book.setUser(person);
        bookRepo.save(book);

        return "redirect:/mybooks";
    }

    @GetMapping("/user_page/{id}")
    public String userPage(@PathVariable Integer id, Model model) {
        Person person = personRepo.findById(id).get();
        List<Book> books = bookRepo.findByUserId(id);
        List<Review> reviews = reviewRepo.findByRecipient(person);
        model.addAttribute("user", person);
        model.addAttribute("books", books);
        model.addAttribute("reviews", reviews);
        return "user_page";
    }

}
