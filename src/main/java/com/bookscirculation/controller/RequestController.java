package com.bookscirculation.controller;

import com.bookscirculation.model.Book;
import com.bookscirculation.model.BookRequest;
import com.bookscirculation.model.Person;
import com.bookscirculation.repos.BookRepo;
import com.bookscirculation.repos.BookRequestRepo;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.service.BookRequestService;
import com.bookscirculation.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@Controller
public class RequestController {

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private PersonService personService;

    @Autowired
    private BookRequestRepo bookRequestRepo;

    @Autowired
    BookRequestService bookRequestService;


    @GetMapping("/books/first_request/{id}")
    public String firstRequestStart(@PathVariable Integer id, Model model) {
        Book book = bookRepo.findById(id).get();
        int userId = personService.getCurrentId();
        Person curUser = personRepo.findById(userId).get();

        BookRequest bookRequest = BookRequest.builder().bookDelivery(false).status("wait").build();

        model.addAttribute("book", book);
        model.addAttribute("firstRequest", bookRequest);
        model.addAttribute("curUser", curUser);
        return "first_req";
    }

    @PostMapping("/books/first_request_save/{id}")
    public String firstRequestStart(@Valid BookRequest bookRequest, @PathVariable Integer id) {
        int userId = personService.getCurrentId();
        Person person = personRepo.findById(userId).get();

        Book book = bookRepo.findById(id).get();

        bookRequest.setStatus("wait");
        bookRequest.setPersonInitiator(person);
        bookRequest.setRespondentsBook(book);

        bookRequestRepo.save(bookRequest);

        bookRequestService.sendMailRequest(book.getUser());

        return "redirect:/books";
    }

    @GetMapping("/my_requests")
    public String getMyRequests(Model model) {
        int userId = personService.getCurrentId();
        Person initiator = personRepo.findById(userId).get();
        List<BookRequest> reqs = bookRequestRepo.findByPersonInitiator(initiator);
        Collections.reverse(reqs);
        model.addAttribute("reqs", reqs);
        return "my_requests";
    }

    @GetMapping("/requests_to_me")
    public String getRequestsToMe(Model model) {
        int userId = personService.getCurrentId();
        List<BookRequest> reqs = bookRequestRepo.findByIdOwner(userId);
        Collections.reverse(reqs);
        model.addAttribute("reqs", reqs);
        return "requests_to_me";
    }
}

