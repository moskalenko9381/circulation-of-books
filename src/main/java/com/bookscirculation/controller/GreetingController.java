package com.bookscirculation.controller;

import com.bookscirculation.model.Person;
import com.bookscirculation.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Slf4j
@Controller
public class GreetingController {

    @Autowired
    private PersonService personService;


    @GetMapping("/")
    public String mainPage() {
        return "main";
    }


    @GetMapping("/sign_up")
    public String signUp(Model model) {
        model.addAttribute("person", new Person());
        return "sign_up";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }


    @PostMapping("/sign_up")
    public String addPerson(@Valid Person person, BindingResult bindingResult, Model model) {

        if (!personService.addPerson(person)) {
            model.addAttribute("message", "User with this email already exists");
            return "sign_up";
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("person", person);
            return "sign_up";
        }

        model.addAttribute("message", "The letter has been sent to your" +
                " mail to confirm registration. Follow the link provided in the letter");
        return "sign_up";
    }

    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = personService.activateUser(code);
        if (isActivated) {
            model.addAttribute("message", "User successfully activated");
        } else {
            model.addAttribute("message", "Activation code is not found");
        }
        return "login";
    }


}