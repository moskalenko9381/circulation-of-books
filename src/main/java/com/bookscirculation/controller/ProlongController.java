package com.bookscirculation.controller;


import com.bookscirculation.model.Prolong;
import com.bookscirculation.repos.DealRepo;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.repos.ProlongRepo;
import com.bookscirculation.service.PersonService;
import com.bookscirculation.service.ProlongService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;

@Controller
@Slf4j
public class ProlongController {

    @Autowired
    ProlongService prolongService;

    @Autowired
    ProlongRepo prolongRepo;

    @Autowired
    DealRepo dealRepo;

    @Autowired
    PersonService personService;

    @Autowired
    PersonRepo personRepo;

    @GetMapping("/prolong")
    public String addProlong(@RequestParam Integer deal, @RequestParam String date) {
        prolongService.addProlong(deal, date);
        return "redirect:/deals";
    }

    @GetMapping("/prolong_requests")
    public String prolongsToMe(Model model) {
        int userId = personService.getCurrentId();
        List<Prolong> prolongs = prolongRepo.findByUser(personRepo.findById(userId).get());
        Collections.reverse(prolongs);
        model.addAttribute("prolongs", prolongs);
        return "prolongs";
    }

    @GetMapping("/prolongs/accept/{id}")
    public String acceptProlong(@PathVariable Integer id) {
        prolongService.acceptProlong(id);
        return "redirect:/prolong_requests";
    }

    @GetMapping("/prolongs/reject/{id}")
    public String rejectProlong(@PathVariable Integer id) {
        Prolong curProlong = prolongRepo.findById(id).get();
        log.info("Prolong was rejected");
        prolongRepo.delete(curProlong);
        return "redirect:/prolong_requests";
    }


}
