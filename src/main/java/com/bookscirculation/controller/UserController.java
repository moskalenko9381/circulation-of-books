package com.bookscirculation.controller;

import com.bookscirculation.model.*;
import com.bookscirculation.repos.BookRepo;
import com.bookscirculation.repos.PersonRepo;
import com.bookscirculation.repos.ReviewRepo;
import com.bookscirculation.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private PersonService personService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ReviewRepo reviewRepo;

    @GetMapping("/myprofile")
    public String booksPage(Model model) {
        Person user = personRepo.findByEmail(personService.currentEmail());
        List<Review> reviews = reviewRepo.findByRecipient(user);
        model.addAttribute("user", user);
        model.addAttribute("reviews", reviews);
        return "profile";
    }

    @GetMapping("/mybooks")
    public String getMyBooks(Model model) {
        int id = personService.getCurrentId();
        List<Book> myBooks = bookRepo.findByUserId(id);
        model.addAttribute("books", myBooks);
        return "my_books";
    }

    @GetMapping("/edit_person")
    public String editProfile(Model model) {
        Person person = personRepo.findByEmail(personService.currentEmail());
        model.addAttribute("person", person);
        return "edit_person";
    }

    @PostMapping("/edit_person")
    public String editPerson(@Valid Person person, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("person", person);
            return "edit_person";
        }

        person.setActive(true);
        person.setRoles(Collections.singleton(Role.USER));
        person.setProvider(Provider.LOCAL);
        personRepo.save(person);

        return "redirect:/myprofile";
    }

    @GetMapping("/change_password_send")
    public String changePasswordSend(Model model) {
        Person person = personRepo.findByEmail(personService.currentEmail());
        personService.changePasswordMail(person);
        model.addAttribute("person", person);
        model.addAttribute("message", "The link to change yor password has been sent to your mail");
        return "edit_person";
    }

    @GetMapping("/change_password")
    public String changePassword(Model model) {
        model.addAttribute("message", "");
        return "change_password";
    }

    @PostMapping("/change_password")
    public String savePassword(@RequestParam String newPass1, @RequestParam String newPass2, Model model) {
        Person person = personRepo.findByEmail(personService.currentEmail());
        if (newPass1.equals(newPass2)) {
            person.setPassword(passwordEncoder.encode(newPass1));
            personRepo.save(person);
            model.addAttribute("message", "Password is changed");
        } else {
            model.addAttribute("message", "Passwords differ");
        }
        return "change_password";
    }


}
